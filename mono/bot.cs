using System;
using System.IO;
using System.Net.Sockets;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace test1 {
  class trackpiece {
    public int type;//0=straight,1=bend
    public double length;//only used if type==0
    public double angle;//only used if type==1
    public int radius;//only used if type==1 why is this integer
    public bool swap;
    public double speedLimit; //Jukka
    //public int serverTrackIndex; //Jukka 
    public List<double> swapPositionList; //Jukka 
    public trackpiece(int t,double l,double a,int r,bool s) {
      swapPositionList=new List<double>();
      type=t;
      length=l;
      angle=a;
      radius=r;
      swap=s;
    }
    public void print() {
      if(type==0) {
        //Console.WriteLine("type: straight\tlength:"+length+"\tswap:"+swap);
      } else {
        //Console.WriteLine("type: bend\tangle:"+angle+"\tradius:"+radius+"\tswap:"+swap);
      }
    }
  }
  class car {
    //Jukka start
    public double maxAcceleration = 10; //when Throttle = 1.0; value need to be found out. Jukka
    public double minAcceleration = -20; //when Throttle = 0.0; value need to be found out. Jukka
    public double measuredMaxAcceleration=0;
    public double measuredMinAcceleration=0;
    public double measuredMaxSpeed = 0;
    public double measuredMaxAngleSpeed = 0;
    //Jukka end
    int logm=10;//log max
    int logc=0;//log current
    public string name;
    double[] angle;
    int[] pieceindex;
    double[] piecedistance;
    public int lanestart;
    public int laneend;
    public int lap;

    double[] speed;//calculate this
    double[] anglespeed;//same
    int[] tick;

    public car(string s) {
      name=s;
      angle=new double[logm];
      pieceindex=new int[logm];
      piecedistance=new double[logm];
      lanestart=0;
      laneend=0;
      lap=0;

      speed=new double[logm];
      anglespeed=new double[logm];
      tick=new int[logm];
      /*for(int i=0;i<logm;i++) {
        angle[i]=0;
        pieceindex[i]=0;
        piecedistance[i]=0;
        speed[i]=0;
        anglespeed[i]=0;
        tick[i]=i-logm;
      }*/
    }
    public double getangle(int a=0) {
      if(a>logm-1) { a=logm-1; }
      return angle[(logc+logm-1-a)%logm];
    }
    public int getpieceI(int a=0) {
      if(a>logm-1) { a=logm-1; }
      return pieceindex[(logc+logm-1-a)%logm];
    }
    public double getpieceD(int a=0) {
      if(a>logm-1) { a=logm-1; }
      return piecedistance[(logc+logm-1-a)%logm];
    }
    public double getspeed(int a=0) {
      if(a>logm-1) { a=logm-1; }
      return speed[(logc+logm-1-a)%logm];
    }
    public double getaspeed(int a=0) {
      if(a>logm-1) { a=logm-1; }
      return angle[(logc+logm-1-a)%logm];
    }
    public int gettick(int a=0) {
      if(a>logm-1) { a=logm-1; }
      return tick[(logc+logm-1-a)%logm];
    }

    public void setangle(double a) {
      angle[logc]=a;
    }
    public void setpieceI(int a) {
      pieceindex[logc]=a;
    }
    public void setpieceD(double a) {
      piecedistance[logc]=a;
    }
    public void setaspeed(double a) {
      anglespeed[logc]=a;
    }
    public void settick(int a) {
      tick[logc]=a;
    }

    public void calculate() {//calculate speed and anglespeed
      int lt=(logc+logm-1)%logm;
      int timeGoneInTicksSincePreviousUpdate = tick[lt] - tick[(lt + logm - 1) % logm];//Jukka
      double previousSpeed = speed[lt];//Jukka
      if(pieceindex[lt]==pieceindex[(lt+logm-1)%logm]) {
        speed[lt]=piecedistance[lt]-piecedistance[(lt+logm-1)%logm];
        speed[lt]/=(tick[lt]-tick[(lt+logm-1)%logm]);
      } else {
        speed[lt]=speed[(lt+logm-1)%logm];
      }
      double currentSpeed = speed[lt];//Jukka
      double acceleration = (previousSpeed - currentSpeed) / timeGoneInTicksSincePreviousUpdate;//Jukka
      anglespeed[lt]=angle[lt]-angle[(lt+logm-1)%logm];

      //Jukka start
      if(currentSpeed > this.measuredMaxSpeed) { this.measuredMaxSpeed = currentSpeed; }
      if(Math.Abs(anglespeed[lt]) > this.measuredMaxAngleSpeed) { this.measuredMaxAngleSpeed = Math.Abs(anglespeed[lt]); }
      if(this.measuredMaxAcceleration < acceleration) { this.measuredMaxAcceleration = acceleration; }
      if(this.measuredMinAcceleration > acceleration) { this.measuredMinAcceleration = acceleration; }
      //Jukka end
    }
    public void lognext() {//call after all data has been read
      logc=(logc+1)%logm;
    }
    public void print(StreamWriter f) {
      int lt=(logc+logm-1)%logm;
      Console.WriteLine("tick:"+tick[lt]+"\tname:"+name+"\tlap:"+lap+"\tlane:"+laneend+
                        "\nangle:"+angle[lt]+
                        "\naSpeed:"+anglespeed[lt]+
                        "\nspeed:"+speed[lt]+
                        "\npieceI:"+pieceindex[lt]+"\tpieceD:"+piecedistance[lt]);
      f.WriteLine(tick[lt]+","+name+","+lap+","+laneend+","+angle[lt]+","+anglespeed[lt]+","+speed[lt]+","+pieceindex[lt]+","+piecedistance[lt]);
    }
  }

  public class Bot {
    Random rand=new Random();
    List<car> cars=new List<car>();
    List<trackpiece> track=new List<trackpiece>();
    List<trackpiece> trackServer=new List<trackpiece>();
    List<int> ownTrackIndexByServerTrackIndexList=new List<int>();

    List<int> lanepenalty=new List<int>();
    bool lanemode=false;

    List<double> maxspd=new List<double>();
    bool change=false;

    //public static StreamWriter file;

    public static string mycar;
    public static string mykey;
    public static string mytrack="france";//"keimola","germany","usa","france"
    public static string mycolor="green";
    public static int startRow=20;
    public void parsepositions(string s) {
      int start,end;
      string str;
      int tick=0;
      if(s.Contains("\"gameTick\":")) {
        start=s.IndexOf("\"gameTick\":")+11;
        end=s.IndexOf('}',start);
        str=s.Substring(start,end-start);
        tick=int.Parse(str);
      }
      string name;
      while(s.Contains("\"name\":")) {
        start=s.IndexOf("\"name\":")+8;
        end=s.IndexOf(',',start);
        name=s.Substring(start,end-start-1);
        for(int i=0;i<cars.Count;i++) {
          if(name!=cars[i].name) { continue; }

          int start2,end2;
          cars[i].settick(tick);

          start2=s.IndexOf("\"angle\":")+8;
          end2=s.IndexOf(',',start2);
          str=s.Substring(start2,end2-start2);
          cars[i].setangle(double.Parse(str));

          start2=s.IndexOf("\"pieceIndex\":")+13;
          end2=s.IndexOf(',',start2);
          str=s.Substring(start2,end2-start2);
          cars[i].setpieceI(int.Parse(str));

          start2=s.IndexOf("\"inPieceDistance\":")+18;
          end2=s.IndexOf(',',start2);
          str=s.Substring(start2,end2-start2);
          cars[i].setpieceD(double.Parse(str));

          start2=s.IndexOf("\"startLaneIndex\":")+17;
          end2=s.IndexOf(',',start2);
          str=s.Substring(start2,end2-start2);
          cars[i].lanestart=int.Parse(str);

          start2=s.IndexOf("\"endLaneIndex\":")+15;
          end2=s.IndexOf('}',start2);
          str=s.Substring(start2,end2-start2);
          cars[i].laneend=int.Parse(str);

          start2=s.IndexOf("\"lap\":")+6;
          end2=s.IndexOf('}',start2);
          str=s.Substring(start2,end2-start2);
          cars[i].lap=int.Parse(str);

          cars[i].lognext();
        }
        s=s.Substring(end);
      }
    }
    public void parsegamestart(string s) {
      int start,end,start2,end2;
      start=s.IndexOf("\"pieces\":")+9;
      end=s.IndexOf(']',start);
      string str=s.Substring(start,end-start);
      while(str.Contains("\"length\":")||str.Contains("\"radius\":")) {
        if(!str.Contains("\"length\":")) {
          start=100000;
        } else {
          start=str.IndexOf("\"length\":")+9;
        }
        if(!str.Contains("\"radius\":")) {
          start2=100000;
        } else {
          start2=str.IndexOf("\"radius\":")+9;
        }
        if(start<start2) {//straight
          end=str.IndexOf('}',start);
          end2=str.IndexOf(',',start);
          if(end==-1) { end=100000; }
          if(end2==-1) { end2=100000; }
          if(end<end2) {
            track.Add(new trackpiece(0,double.Parse(str.Substring(start,end-start)),0.0d,0,false));
          } else {
            track.Add(new trackpiece(0,double.Parse(str.Substring(start,end2-start)),0.0d,0,true));
          }
        } else {//bend
          int radius;
          end=str.IndexOf(',',start2);
          radius=int.Parse(str.Substring(start2,end-start2));
          start2=str.IndexOf("\"angle\":")+8;
          end=str.IndexOf('}',start2);
          end2=str.IndexOf(',',start2);
          if(end==-1) { end=100000; }
          if(end2==-1) { end2=100000; }
          if(end<end2) {
            track.Add(new trackpiece(1,0,double.Parse(str.Substring(start2,end-start2)),radius,false));
          } else {
            track.Add(new trackpiece(1,0,double.Parse(str.Substring(start2,end2-start2)),radius,true));
          }
        }
        str=str.Substring(end);
      }
      //Jukka Start (following rows are added by Jukka - until "//Jukka End")
        //unite trackpieces
      trackServer=track;
      List<trackpiece> trackTemp = new List<trackpiece>();
      trackpiece previousTrackPiece = track[0];
      ownTrackIndexByServerTrackIndexList.Add(trackTemp.Count);
      //trackTemp.Add(previousTrackPiece);
      for (int i = 1; i < track.Count; i++)
      {
          trackpiece trackPiece = track[i];
          ownTrackIndexByServerTrackIndexList.Add(trackTemp.Count);
          if (trackPiece.type == previousTrackPiece.type && trackPiece.radius == previousTrackPiece.radius && Math.Sign(trackPiece.angle) == Math.Sign(previousTrackPiece.angle))
          {
              //trackPiece is same kind as previousTrackPiece
              if (trackPiece.swap == true)
              { previousTrackPiece.swap = true; previousTrackPiece.swapPositionList.Add(previousTrackPiece.length); } //swapPosition is in the start point of the swap
              previousTrackPiece.angle += trackPiece.angle;
              previousTrackPiece.length += trackPiece.length;
          }
          else
          {
              //trackPiece is different kind as previousTrackPiece
              if (trackPiece.swap == true)
              { trackPiece.swapPositionList.Add(0); } //swapPosition is in the start point of the swap
              trackTemp.Add(previousTrackPiece);
              previousTrackPiece = trackPiece;
          }
      }
      trackTemp.Add(previousTrackPiece);
      track = trackTemp;  
        //uniting trackpices finished

        //calculate speedlimits of trackpieces 
        //type 0=straight, 1=bend
        double speedLimitForBendWhichRadiusIs100 = 100; //all bend's speedlimit will be calculated after this (for example: two times bigger radius -> two times bigger speedlimit)
      for (int i = 0; i < track.Count; i++)
      {
          trackpiece trackPiece = track[i];
          if (trackPiece.type == 0)
          {
              //straight
              trackPiece.speedLimit = 10000;
          }
          else
          {
              //bend
              trackPiece.speedLimit = speedLimitForBendWhichRadiusIs100 * trackPiece.radius / 100;
          }
      }
      //Jukka End

      start=s.IndexOf("\"lanes\":")+8;
      s=s.Substring(start);
      while(s.Contains("\"distanceFromCenter\":")) {
        start=s.IndexOf("\"distanceFromCenter\":")+21;
        end=s.IndexOf(',',start);
        lanepenalty.Add(int.Parse(s.Substring(start,end-start)));
        s=s.Substring(end);
      }

      start=s.IndexOf("\"cars\":")+7;
      s=s.Substring(start);
      while(s.Contains("\"name\":")) {
        start=s.IndexOf("\"name\":")+8;
        end=s.IndexOf(',',start);
        cars.Add(new car(s.Substring(start,end-start-1)));
        s=s.Substring(end);
      }
    }

    public bool wantedlane(int i) {//false=left,true=right
      i=(i+1)%trackServer.Count;
      while(!trackServer[i].swap) { i=(i+1)%trackServer.Count; }
      while(trackServer[i].type==0) { i=(i+1)%trackServer.Count; }
      double a=trackServer[i].angle;
      i=(i+1)%trackServer.Count;
      while(!trackServer[i].swap) {
        a+=trackServer[i].angle;
        i=(i+1)%trackServer.Count;
      }
      if(a<0) { return false; } else { return true; }
    }

    public static void Main(string[] args) {
      string host = args[0];
      int port = int.Parse(args[1]);
      string botName = args[2];
      string botKey = args[3];
      //mycar=botName;
      mycar="Team Kyamk TI12";
      mykey=botKey;
      //Console.Clear();
      //Console.SetCursorPosition(1, startRow +  1);      
      //Console.WriteLine("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

      //file=new StreamWriter("eea.txt");
      //file.WriteLine("track:"+mytrack);

      using(TcpClient client = new TcpClient(host,port)) {
        NetworkStream stream = client.GetStream();
        StreamReader reader = new StreamReader(stream);
        StreamWriter writer = new StreamWriter(stream);
        writer.AutoFlush = true;

        new Bot(reader,writer,new Join(botName,botKey));
      }
      //file.Close();
    }
    private StreamWriter writer;

    Bot(StreamReader reader,StreamWriter writer,Join join) {
      this.writer = writer;
      string line,line2;

      send(join);
      //Console.SetCursorPosition(1, startRow +  2);
      //writer.WriteLine("{\"msgType\":\"joinRace\",\"data\":{\"botId\":{\"name\":\""+mycar+"\",\"key\":\""+mykey+"\",\"color\":\""+mycolor+"\"},\"trackName\":\""+mytrack+"\",\"carCount\":1}}");
      //writer.WriteLine("{\"msgType\":\"joinRace\",\"data\":{\"botId\":{\"name\":\""+mycar+"\",\"key\":\""+mykey+"\",\"color\":\""+mycolor+"\"},\"trackName\":\""+mytrack+",\"carCount\":2\"}}");
      ////Console.ReadKey();
      while((line = reader.ReadLine()) != null) {
        line2=line;
        MsgWrapper msg = JsonConvert.DeserializeObject<MsgWrapper>(line);
        switch(msg.msgType) {
          case "carPositions":
            ////Console.Clear();
            //Jukka : personal code starts
            //send(new Throttle(0.50f+rand.Next(250)*0.001f));//0.6+rand.Next(10)*0.01f
            parsepositions(line);
            
            for(int i=0;i<cars.Count;i++) {
              
              //cars[i].calculate();
              //if(cars[i].gettick()%10==0) {//print info every 10 ticks
              //Console.SetCursorPosition(1, startRow +  20+(i-1)*3);
                //cars[i].print(file);
                //int serverTrackPieceIndex = cars[i].getpieceI();
                //int ownTrackPieceIndex = ownTrackIndexByServerTrackIndexList[serverTrackPieceIndex];
                //Console.Write("server piece index: " + serverTrackPieceIndex + ", own piece index: " + ownTrackPieceIndex + "\n");
                //trackServer[serverTrackPieceIndex].print();
                //track[cars[i].getpieceI()].print();
              //}
              if(cars[i].name==mycar){
                cars[i].calculate();
                ////Console.Write(cars[i].getspeed());
                double t=1.0d;//1.0d;
                int thispiece=cars[i].getpieceI();
                int nextpiece=(thispiece+1)%trackServer.Count;
                if(trackServer[(nextpiece+1)%trackServer.Count].type==1) {
                  //if(cars[i].getspeed()>maxspd[(nextpiece+1)%trackServer.Count]) {
                    t*=0.75d;
                  //}
                    if(trackServer[(nextpiece+1)%trackServer.Count].radius<101) {
                      t*=0.75d;
                    }
                }
                if(trackServer[nextpiece].type==1) {
                  //if(cars[i].getspeed()>maxspd[nextpiece]) {
                    t*=0.7d;
                  //}
                    if(trackServer[nextpiece].radius<101) {
                      t*=0.5d;
                    }
                }
                if(trackServer[thispiece].type==1) {
                  t*=0.9d;
                  if(Math.Abs(cars[i].getaspeed())>Math.Abs(cars[i].getaspeed(1))) {
                    t*=0.5d;
                    if(Math.Abs(cars[i].getangle())>15) {
                      t*=0.5d;
                      //change=true;
                    }
                  }
                  /*if(Math.Abs(cars[i].getaspeed())<Math.Abs(cars[i].getaspeed(1))) {
                    if(Math.Abs(cars[i].getangle())<5) {
                      t+=0.5d;
                      if(t>0.7d) { t=0.7d; }
                    }
                  }*/
                }
                /*if(Math.Abs(cars[i].getaspeed())>Math.Abs(cars[i].getaspeed(1))) {
                  t=0.1d;
                }*/
                //if(cars[i].getspeed()>8) {//global max speed
                  //t=0.05d;
                //}
                if(cars[i].getspeed()<3) {
                  //t=1.0d;
                }
                if(Math.Abs(cars[i].getangle())>20) {
                  //maxspd[thispiece]=cars[i].getspeed()*0.8d;
                  t=0.0d;
                }
                //if(cars[i].getpieceI()!=cars[i].getpieceI(1)) {
                //if(cars[i].gettick()%2==1){
                if((cars[i].gettick()%30)==0){
                  if(change) {
                    maxspd[(thispiece-1+trackServer.Count)%trackServer.Count]+=0.25d;
                  }
                  Console.Write("\ntick:"+cars[i].gettick()+" piece:"+cars[i].getpieceI()+" throttle:"+t);
                  change=false;
                  if(!wantedlane(cars[i].getpieceI())) {
                    writer.WriteLine("{\"msgType\":\"switchLane\",\"data\":\"Left\"}");
                    //Console.WriteLine("lane LEFT");
                    //lanemode=false;
                  } else {
                    writer.WriteLine("{\"msgType\":\"switchLane\",\"data\":\"Right\"}");
                    //Console.WriteLine("lane RIGHT");
                    //lanemode=true;
                  }
                } else {
                  //Console.WriteLine("throttle: "+t+"          ");
                  //Console.Write("\n");
                  if(cars[i].getspeed()>8) {
                    //t=0.0d;
                  }
                  send(new Throttle(t));
                }
                //}
                //for(int j=0;j<trackServer.Count;j++) {
                  //Console.Write(j+":"+maxspd[j]+"\t ");
                //}
                //Console.Write("\n\n\n");
                /*if(track[(cars[i].getpieceI()+1)%track.Count].type==1) {
                  send(new Throttle(0.50f));
                } else {
                  send(new Throttle(0.95f));
                }*/
              }
            }
            break;
          case "join":
            //Console.SetCursorPosition(1, startRow +  30);
            //Console.WriteLine("Joined");
            //send(new Ping());
            break;
          case "gameInit":
            //Console.SetCursorPosition(1, startRow +  31);
            //Console.WriteLine("Race init");
            //send(new Ping());
            parsegamestart(line);
            //Console.WriteLine("total cars: "+cars.Count);
            for(int i=0;i<cars.Count;i++) {
              //Console.WriteLine(i+": "+cars[i].name);
            }
            //Console.WriteLine("trackpieces: "+track.Count);
            //file.WriteLine("TRACKDATA:");
            //file.WriteLine("index,type,length,angle,radius,swap");
            for(int i=0;i<track.Count;i++) {
              //Console.Write("index: "+i+" ");
              //track[i].print();
              //file.WriteLine(i+","+track[i].type+","+track[i].length+","+track[i].angle+","+track[i].radius+","+track[i].swap);
            }
            //file.WriteLine("\nPOSITIONDATA:");
            //file.WriteLine("tick,name,lap,lane,angle,anglespeed,speed,pieceindex,piecedistance");
            //Console.WriteLine("\nLANES:");
            for(int i=0;i<lanepenalty.Count;i++) {
              //Console.WriteLine(i+": "+lanepenalty[i]);
            }

            for(int i=0;i<trackServer.Count;i++) {
              maxspd.Add(4.0d);
            }
            //Console.Write(line);
            break;
          case "gameEnd":
            //Console.SetCursorPosition(1, startRow +  50);
            //Console.WriteLine("Race ended");
            //Console.WriteLine("Measures max acceleration: "+cars[0].measuredMaxAcceleration);
            //Console.WriteLine("Measures min acceleration: " + cars[0].measuredMinAcceleration);
            //Console.WriteLine("Measures max speed       : " + cars[0].measuredMaxSpeed);
            //Console.SetCursorPosition(1, startRow +  60);
            //send(new Ping());
            break;
          case "gameStart":
            send(new Throttle(1.0d));
            //Console.SetCursorPosition(1, startRow +  3);
            //Console.WriteLine("Race starts");
            break;
          case "lapFinished":
            //Console.SetCursorPosition(1, startRow +  4);
            //Console.WriteLine("lap done");
            ////Console.Write("EXIT");
            //Environment.Exit(0);
            break;
          case "crash":
            //Console.SetCursorPosition(1, startRow +  5);
            //Console.WriteLine("crash!");
            //Console.Write("EXIT");
            //Environment.Exit(0);
            break;
          default:
            //Console.SetCursorPosition(1, startRow +  7);
            //Console.WriteLine("unknown message:\n"+line);
            break;
            //Jukka : personal code ends
        }
      }
    }

    private void send(SendMsg msg) {
      writer.WriteLine(msg.ToJson());
    }
  }

  class MsgWrapper {
    public string msgType;
    public Object data;

    public MsgWrapper(string msgType,Object data) {
      this.msgType = msgType;
      this.data = data;
    }
  }

  abstract class SendMsg {
    public string ToJson() {
      return JsonConvert.SerializeObject(new MsgWrapper(this.MsgType(),this.MsgData()));
    }
    protected virtual Object MsgData() {
      return this;
    }

    protected abstract string MsgType();
  }

  class Join:SendMsg {
    public string name;
    public string key;
    public string color;

    public Join(string name,string key) {
      this.name = name;
      this.key = key;
      this.color = "yellow";
    }

    protected override string MsgType() {
      return "join";
    }
  }

  class Ping:SendMsg {
    protected override string MsgType() {
      return "ping";
    }
  }

  class Throttle:SendMsg {
    public double value;

    public Throttle(double value) {
      this.value = value;
      if(value<0) { value=0.0d; }
      if(value>1) { value=1.0d; }
    }

    protected override Object MsgData() {
      return this.value;
    }

    protected override string MsgType() {
      return "throttle";
    }
  }
}